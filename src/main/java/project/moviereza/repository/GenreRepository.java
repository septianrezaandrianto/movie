package project.moviereza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.moviereza.model.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

}
