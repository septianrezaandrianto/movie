package project.moviereza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.moviereza.model.MovieGenre;

@Repository
public interface MovieGenreRepository extends JpaRepository<MovieGenre, Long>{

}
