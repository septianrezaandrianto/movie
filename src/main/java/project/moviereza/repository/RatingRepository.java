package project.moviereza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.moviereza.model.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long>{

}
