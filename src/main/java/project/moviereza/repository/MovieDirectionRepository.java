package project.moviereza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.moviereza.model.MovieDirection;

@Repository
public interface MovieDirectionRepository extends JpaRepository<MovieDirection, Long>{

}
