package project.moviereza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.moviereza.model.Actor;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Long> {

}
