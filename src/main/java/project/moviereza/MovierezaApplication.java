package project.moviereza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovierezaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovierezaApplication.class, args);
	}

}
