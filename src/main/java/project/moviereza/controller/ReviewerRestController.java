package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.ReviewerDTO;
import project.moviereza.model.Reviewer;
import project.moviereza.repository.ReviewerRepository;

@RestController
@RequestMapping("/api")
public class ReviewerRestController {
	
	@Autowired
	private ReviewerRepository reviewerRepository;
	
//	Melihat list Reviewer
	@GetMapping("/reviewers/all")
	public HashMap<String, Object> getAllReviewers() {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		ArrayList<ReviewerDTO> listReviewerDto = new ArrayList<ReviewerDTO>();
		
		for (Reviewer rev: reviewerRepository.findAll()) {
			ReviewerDTO revDto = new ReviewerDTO(rev.getRevId(), rev.getRevName());
			listReviewerDto.add(revDto);
		}
		
		hasReviewer.put("Message", "Show All Data");
		hasReviewer.put("Total", listReviewerDto.size());
		hasReviewer.put("Data", listReviewerDto);
		
	return hasReviewer;
	}
	
//	Melihat Reviewer berdasarkan id
	@GetMapping("/reviewers/{id}")
	public HashMap<String, Object> getReviewerById(@PathVariable(value="id") Long revId) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
		ReviewerDTO revDto = new ReviewerDTO(rev.getRevId(), rev.getRevName());
		
		hasReviewer.put("Message" , "Data By Id");
		hasReviewer.put("Data", revDto);
		
	return hasReviewer;
	}
	
//	Menghapus reviewer
	@DeleteMapping("/reviewers/delete/{id}")
	public HashMap<String, Object> deleteReviewerById(@PathVariable(value="id") Long revId) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
		reviewerRepository.delete(rev);
		
		hasReviewer.put("Message", "Delete success");
		hasReviewer.put("Data", rev);
		
	return hasReviewer;
	}
	
//	Menambahkan seorang reviewer
	@PostMapping("/reviewers/add")
	public HashMap<String, Object> addReviewer(@Valid @RequestBody ReviewerDTO reviewerDTO) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
		Reviewer rev = new Reviewer(reviewerDTO.getRevId(), reviewerDTO.getRevName());
	
		hasReviewer.put("Message", "Add success");
		hasReviewer.put("Data", reviewerRepository.save(rev));
		
	return hasReviewer;
	}
	
//	Menambahkan banyak reviewers
	@PostMapping("/reviewers/add/more")
	public HashMap<String, Object> addMoreReviewers(@Valid @RequestBody ReviewerDTO...reviewerDTOs) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		ArrayList<Reviewer> listReviewer = new ArrayList<Reviewer>();
		
		for (ReviewerDTO revDto : reviewerDTOs) {
			Reviewer rev = new Reviewer(revDto.getRevId(), revDto.getRevName());
			
			reviewerRepository.save(rev);
			listReviewer.add(rev);						
		}
		
		hasReviewer.put("Message" , "Add Success");
		hasReviewer.put("Total Insert", listReviewer.size());
		hasReviewer.put("Data", listReviewer);
		
	return hasReviewer;
	}
	
//	Update data Reviewer
	@PutMapping("/reviewers/update/{id}")
	public HashMap<String, Object> updateReviewer(@PathVariable(value="id") Long revId, @Valid @RequestBody ReviewerDTO reviewerDTO) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
		rev.setRevName(reviewerDTO.getRevName());
		
		
		hasReviewer.put("Message", "Update success");
		hasReviewer.put("Data", reviewerRepository.save(rev));
		
	return hasReviewer;
	}

}
