package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.GenreDTO;
import project.moviereza.model.Genre;
import project.moviereza.repository.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreRestController {

	@Autowired
	private GenreRepository genreRepository;
	
//	Melihat list Genres
	@GetMapping("/genres/all")
	public HashMap<String, Object> getAllGenres() {
		
		HashMap<String, Object> hasGenres = new HashMap<String, Object>();
		ArrayList<GenreDTO> listGenreDto = new ArrayList<GenreDTO>();
		
		for (Genre gen: genreRepository.findAll()) {
			GenreDTO genDto = new GenreDTO(gen.getGenId(), gen.getGenTitle());
			listGenreDto.add(genDto);
		}
		
		hasGenres.put("Message", "Show All Data");
		hasGenres.put("Total", listGenreDto.size());
		hasGenres.put("Data", listGenreDto);
		
	return hasGenres;
	}
	
//	Melihat genre berdasarkan id
	@GetMapping("/genres/{id}")
	public HashMap<String, Object> getGenreById(@PathVariable(value="id") Long genId) {
		
		HashMap<String, Object> hasGenre = new HashMap<String, Object>();
		
		Genre gen = genreRepository.findById(genId).orElse(null);
		
		GenreDTO genDto = new GenreDTO(gen.getGenId(), gen.getGenTitle());
		
		hasGenre.put("Message" , "Data By Id");
		hasGenre.put("Data", genDto);
		
	return hasGenre;
	}
	
//	Menambahkan sebuah genre
	@PostMapping("/genres/add")
	public HashMap<String, Object> addGenre(@Valid @RequestBody GenreDTO genreDTO) {
		
		HashMap<String, Object> hasGenre = new HashMap<String, Object>();
		Genre gen = new Genre(genreDTO.getGenId(), genreDTO.getGenTitle());
		
		hasGenre.put("Message", "Add success");
		hasGenre.put("Data", genreRepository.save(gen));
		
	return hasGenre;
	}
	
//	Menambahkan banyak genres
	@PostMapping("/genres/add/more")
	public HashMap<String, Object> addMoreGenres(@Valid @RequestBody GenreDTO...genreDTOs) {
		
		HashMap<String, Object> hasGenre = new HashMap<String, Object>();
		ArrayList<Genre> listGenres = new ArrayList<Genre>();
		
		for (GenreDTO genDto : genreDTOs ) {
			Genre gen = new Genre(genDto.getGenId(), genDto.getGenTitle());
			genreRepository.save(gen);
			listGenres.add(gen);						
		}
		
		hasGenre.put("Message" , "Add Success");
		hasGenre.put("Total Insert", listGenres.size());
		hasGenre.put("Data", listGenres);
		
	return hasGenre;
	}
	
//	Update data genres
	@PutMapping("/genres/update/{id}")
	public HashMap<String, Object> updateGenres(@PathVariable(value="id") Long genId, @Valid @RequestBody GenreDTO genreDTO) {
		
		HashMap<String, Object> hasGenre = new HashMap<String, Object>();
		
		Genre gen = genreRepository.findById(genId).orElse(null);
	
			gen.setGenTitle(genreDTO.getGenTitle());
		
		hasGenre.put("Message", "Update success");
		hasGenre.put("Data", genreRepository.save(gen));
		
	return hasGenre;
	}
	
	
}
