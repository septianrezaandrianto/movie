package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.RatingDTO;
import project.moviereza.model.Rating;
import project.moviereza.repository.RatingRepository;

@RestController
@RequestMapping("/api")
public class RatingRestController {

	@Autowired
	private RatingRepository ratingRepository;
	
//	Melihat list Rating
	@GetMapping("/ratings/all")
	public HashMap<String, Object> getAllRatings() {
		
		HashMap<String, Object> hasRating = new HashMap<String, Object>();
		
		ArrayList<RatingDTO> listRating = new ArrayList<RatingDTO>();
		
		for (Rating rat : ratingRepository.findAll()) {
			RatingDTO ratDto = new RatingDTO(rat.getId(), rat.getMovie(), rat.getReviewer(), rat.getRevStars(), rat.getNumORatings());
			listRating.add(ratDto);
		}
		
		hasRating.put("Message", "Show All Data");
		hasRating.put("Total", listRating.size());
		hasRating.put("Data", listRating);
		
	return hasRating;
	}
	
//	Melihat Rating berdasarkan id
	@GetMapping("/ratings/{id}")
	public HashMap<String, Object> getRatingById(@PathVariable(value="id") Long id) {
		
		HashMap<String, Object> hasRating = new HashMap<String, Object>();
		
		Rating rat = ratingRepository.findById(id).orElse(null);
		
		RatingDTO ratDto = new RatingDTO(rat.getId(), rat.getMovie(), rat.getReviewer(), rat.getRevStars(), rat.getNumORatings());
		
		hasRating.put("Message", "Data By Id");
		hasRating.put("Data", ratDto);
		
	return hasRating;
	}
	
//	Menghapus rating
	@DeleteMapping("/ratings/delete/{id}")
	public HashMap<String, Object> deleteRatingById(@PathVariable(value="id") Long id) {
	
		HashMap<String, Object> hasRating = new HashMap<String, Object>();
		
		Rating rat = ratingRepository.findById(id).orElse(null);
		
		ratingRepository.delete(rat);
		
		hasRating.put("Message", "Delete Success");
		hasRating.put("Data", rat);
		
	return hasRating;	
	}
	
//	Menambahkan sebuah rating
	@PostMapping("/ratings/add")
	public HashMap<String, Object> addRating(@Valid @RequestBody RatingDTO ratingDTO) {
		
		HashMap<String, Object> hasRating = new HashMap<String, Object>();
		
		Rating rat = new Rating(ratingDTO.getId(), ratingDTO.getMovie(), ratingDTO.getReviewer(), ratingDTO.getRevStars(), ratingDTO.getNumORatings());
		
		hasRating.put("Message", "Add success");
		hasRating.put("Data", ratingRepository.save(rat));
		
	return hasRating;
	}
	
//	Menambah banyak rating
	@PostMapping("/ratings/add/more")
	public HashMap<String, Object> addMoreRatings(@Valid @RequestBody RatingDTO...ratingDTOs) {
		
		HashMap<String, Object> hasRating = new HashMap<String, Object>();
		ArrayList<Rating> listRatings = new ArrayList<Rating>();
		
		for (RatingDTO ratDto : ratingDTOs) {
			Rating rat = new Rating(ratDto.getId(), ratDto.getMovie(), ratDto.getReviewer(), ratDto.getRevStars(), ratDto.getNumORatings());
			
			ratingRepository.save(rat);
			listRatings.add(rat);
		}
		
		hasRating.put("Message", "Add Success");
		hasRating.put("Total Insert", listRatings.size());
		hasRating.put("Data", listRatings);
		
	return hasRating;		
	}
	
//	Update data Rating
	@PutMapping("ratings/update/{id}")
	public HashMap<String, Object> updateRating(@PathVariable(value="id") Long id, @Valid @RequestBody RatingDTO ratingDTO) {
		
		HashMap<String, Object> hasRating = new HashMap<String, Object>();
		
		Rating rat = ratingRepository.findById(id).orElse(null);
		
		if (ratingDTO.getMovie() != null) {
			rat.setMovie(ratingDTO.getMovie());
		}
		
		if (ratingDTO.getReviewer() != null) {
			rat.setReviewer(ratingDTO.getReviewer());
		}
		
		if (ratingDTO.getRevStars() != 0) {
			rat.setRevStars(ratingDTO.getRevStars());
		}
		
		if (ratingDTO.getNumORatings() != 0 ) {
			rat.setNumORatings(ratingDTO.getNumORatings());
		}
		
		hasRating.put("Message", "Update success");
		hasRating.put("Data", ratingRepository.save(rat));
		
	return hasRating;
	}
	
}
