package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.ActorDTO;
import project.moviereza.model.Actor;
import project.moviereza.repository.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorRestController {

	@Autowired
	private ActorRepository actorRepository;
	
//	Melihat list Actors
	@GetMapping("/actors/all")
	public HashMap<String, Object> getAllActors() {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
		ArrayList<ActorDTO> listActorDto = new ArrayList<ActorDTO>();
		
		for (Actor act: actorRepository.findAll()) {
			ActorDTO actDto = new ActorDTO(act.getActId(), act.getActFname(), act.getActLname(), act.getActGender());
			listActorDto.add(actDto);
		}
		
		hashActors.put("Message", "Show All Data");
		hashActors.put("Total", listActorDto.size());
		hashActors.put("Data", listActorDto);
		
	return hashActors;
	}
	
//	Melihat actor berdasarkan id
	@GetMapping("/actors/{id}")
	public HashMap<String, Object> getActorById(@PathVariable(value="id") Long actId) {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
		
		ActorDTO actDto = new ActorDTO(act.getActId(), act.getActFname(), act.getActLname(), act.getActGender());
		hashActors.put("Message" , "Data By Id");
		hashActors.put("Data", actDto);
		
	return hashActors;
	}
	
//	Menghapus actor
	@DeleteMapping("/actors/delete/{id}")
	public HashMap<String, Object> deleteActorById(@PathVariable(value="id") Long actId) {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
		
		actorRepository.delete(act);
		
		hashActors.put("Message", "Delete success");
		hashActors.put("Data",act);
		
	return hashActors;
	}
	
//	Menambahkan seorang actor
	@PostMapping("/actors/add")
	public HashMap<String, Object> addActor(@Valid @RequestBody ActorDTO actorDTO) {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
		Actor act = new Actor(actorDTO.getActId(), actorDTO.getActFname(), actorDTO.getActLname(), actorDTO.getActGender());
	
		
		hashActors.put("Message", "Add success");
		hashActors.put("Data", actorRepository.save(act));
		
	return hashActors;
	}
	
//	Menambahkan banyak actors
	@PostMapping("/actors/add/more")
	public HashMap<String, Object> addMoreActors(@Valid @RequestBody ActorDTO...actorDTOs) {
		
		HashMap<String, Object> hasActor = new HashMap<String, Object>();
		ArrayList<Actor> listActor = new ArrayList<Actor>();
		
		for (ActorDTO actDto : actorDTOs) {
			Actor act =  new Actor(actDto.getActId(), actDto.getActFname(), actDto.getActLname(), actDto.getActGender());
			actorRepository.save(act);
			listActor.add(act);						
		}
		
		hasActor.put("Message" , "Add Success");
		hasActor.put("Total Insert", listActor.size());
		hasActor.put("Data", listActor);
		
	return hasActor;
	}
	
//	Update data actors
	@PutMapping("/actors/update/{id}")
	public HashMap<String, Object> updateActor(@PathVariable(value="id") Long actId, @Valid @RequestBody ActorDTO actorDTO) {
		
		HashMap<String, Object> hasActor = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
		
		actorDTO.setActId(act.getActId());
		
		if(actorDTO.getActFname() != null) {
			act.setActFname(actorDTO.getActFname());
		}
		if(actorDTO.getActLname() != null) {
			act.setActLname(actorDTO.getActLname());
		}
		if(actorDTO.getActGender() != null) {
			act.setActGender(actorDTO.getActGender());
		}
		
		hasActor.put("Message", "Update success");
		hasActor.put("Data", actorRepository.save(act));
		
	return hasActor;
	}
}
