package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.DirectorDTO;
import project.moviereza.model.Director;
import project.moviereza.repository.DirectorRepository;

@RestController
@RequestMapping("/modrepapi")
public class DirectorMapperController {

	@Autowired
	private DirectorRepository directorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	
//	Convert Entity To DTO
	private DirectorDTO convertToDTO(Director director) {
		DirectorDTO dirDto = modelMapper.map(director, DirectorDTO.class);
	return dirDto;
	}
	
//	Convert DTO To Entity
	private Director convertToEntity(DirectorDTO directorDTO) {
		Director dir = modelMapper.map(directorDTO, Director.class);
	return dir;
	}
	
//	Melihat list Directors
	@GetMapping("/directors/all")
	public HashMap<String, Object> getAllDirectors() {
		
		HashMap<String, Object> hashDirectors = new HashMap<String, Object>();
		ArrayList<DirectorDTO> listDirectorDto = new ArrayList<DirectorDTO>();
		
		
		for (Director dir: directorRepository.findAll()) {
								// Mapping dengan model mapper	
			listDirectorDto.add(convertToDTO(dir));
		}
		
		hashDirectors.put("Message", "Show All Data");
		hashDirectors.put("Total", listDirectorDto.size());
		hashDirectors.put("Data", listDirectorDto);
		
	return hashDirectors;
	}
	
//	Melihat Director berdasarkan id
	@GetMapping("/directors/{id}")
	public HashMap<String, Object> getDirectorById(@PathVariable(value="id") Long dirId) {
		
		HashMap<String, Object> hasDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
							// Mapping dengan model mapper	
		DirectorDTO dirDto = convertToDTO(dir);
		
		hasDirector.put("Message" , "Data By Id");
		hasDirector.put("Data", dirDto);
		
	return hasDirector;
	}
	
//	Menambahkan seorang director
	@PostMapping("/directors/add")
	public HashMap<String, Object> addDirector(@Valid @RequestBody DirectorDTO directorDTO) {
		
		HashMap<String, Object> hasDirectors = new HashMap<String, Object>();
//		Mapping dengan model mapper	
		Director director = convertToEntity(directorDTO);
	
		hasDirectors.put("Message", "Add success");
		hasDirectors.put("Data", directorRepository.save(director));
		
	return hasDirectors;
	}
	
//	Update data Directors
	@PutMapping("/directors/update/{id}")
	public HashMap<String, Object> updateDirector(@PathVariable(value="id") Long dirId, @Valid @RequestBody DirectorDTO directorDTO) {
		
		HashMap<String, Object> hasDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
				
		directorDTO.setDirId(dir.getDirId());
		
//		Mapping dengan model mapper	
		if (directorDTO.getDirFname() != null) {
			dir.setDirFname(convertToEntity(directorDTO).getDirFname());
		}
		if (directorDTO.getDirLname() != null) {
			dir.setDirLname(convertToEntity(directorDTO).getDirLname());
		}	
		
		hasDirector.put("Message", "Update success");
		hasDirector.put("Data", directorRepository.save(dir));
		
	return hasDirector;
	}
	
//	Menghapus Director
	@DeleteMapping("/directors/delete/{id}")
	public HashMap<String, Object> deleteDirectorById(@PathVariable(value="id") Long dirId) {
		
		HashMap<String, Object> hasDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
		
		directorRepository.delete(dir);
		
		hasDirector.put("Message", "Delete success");
		hasDirector.put("Data",dir);
		
	return hasDirector;
	}
	
}
