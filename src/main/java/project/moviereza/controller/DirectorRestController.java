package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.DirectorDTO;
import project.moviereza.model.Director;
import project.moviereza.repository.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorRestController {

	@Autowired
	private DirectorRepository directorRepository;
	
//	Melihat list Directors
	@GetMapping("/directors/all")
	public HashMap<String, Object> getAllDirectors() {
		
		HashMap<String, Object> hashDirectors = new HashMap<String, Object>();
		ArrayList<DirectorDTO> listDirectorDto = new ArrayList<DirectorDTO>();
		
		for (Director dir: directorRepository.findAll()) {
			DirectorDTO dicDto = new DirectorDTO(dir.getDirId(), dir.getDirFname(), dir.getDirLname());
			listDirectorDto.add(dicDto);
		}
		
		hashDirectors.put("Message", "Show All Data");
		hashDirectors.put("Total", listDirectorDto.size());
		hashDirectors.put("Data", listDirectorDto);
		
	return hashDirectors;
	}
	
//	Melihat Director berdasarkan id
	@GetMapping("/directors/{id}")
	public HashMap<String, Object> getDirectorById(@PathVariable(value="id") Long dirId) {
		
		HashMap<String, Object> hasDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
		
		DirectorDTO dirDto = new DirectorDTO(dir.getDirId(), dir.getDirFname(), dir.getDirLname());
		
		hasDirector.put("Message" , "Data By Id");
		hasDirector.put("Data", dirDto);
		
	return hasDirector;
	}
	
//	Menghapus Director
	@DeleteMapping("/directors/delete/{id}")
	public HashMap<String, Object> deleteDirectorById(@PathVariable(value="id") Long dirId) {
		
		HashMap<String, Object> hasDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
		
		directorRepository.delete(dir);
		
		hasDirector.put("Message", "Delete success");
		hasDirector.put("Data",dir);
		
	return hasDirector;
	}
	
//	Menambahkan seorang director
	@PostMapping("/directors/add")
	public HashMap<String, Object> addDirector(@Valid @RequestBody DirectorDTO directorDTO) {
		
		HashMap<String, Object> hasDirectors = new HashMap<String, Object>();
		
		Director dir = new Director(directorDTO.getDirId(), directorDTO.getDirFname(), directorDTO.getDirLname());
	
		hasDirectors.put("Message", "Add success");
		hasDirectors.put("Data", directorRepository.save(dir));
		
	return hasDirectors;
	}
	
//	Menambahkan banyak directors
	@PostMapping("/directors/add/more")
	public HashMap<String, Object> addMoreDirectors(@Valid @RequestBody DirectorDTO...directorDTOs) {
		
		HashMap<String, Object> hasDirector = new HashMap<String, Object>();
		ArrayList<Director> listDirector = new ArrayList<Director>();
		
		for (DirectorDTO dirDto : directorDTOs) {
			Director dir = new Director(dirDto.getDirId(), dirDto.getDirFname(), dirDto.getDirLname());
			
			directorRepository.save(dir);
			listDirector.add(dir);						
		}
		
		hasDirector.put("Message" , "Add Success");
		hasDirector.put("Total Insert", listDirector.size());
		hasDirector.put("Data", listDirector);
		
	return hasDirector;
	}
	
//	Update data Directors
	@PutMapping("/directors/update/{id}")
	public HashMap<String, Object> updateDirector(@PathVariable(value="id") Long dirId, @Valid @RequestBody DirectorDTO directorDTO) {
		
		HashMap<String, Object> hasDirector = new HashMap<String, Object>();
		
		Director dir = directorRepository.findById(dirId).orElse(null);
				
		directorDTO.setDirId(dir.getDirId());
		
		if (directorDTO.getDirFname() != null) {
			dir.setDirFname(directorDTO.getDirFname());
		}
		if (directorDTO.getDirLname() != null) {
			dir.setDirLname(directorDTO.getDirLname());
		}
		
		hasDirector.put("Message", "Update success");
		hasDirector.put("Data", directorRepository.save(dir));
		
	return hasDirector;
	}
}
