package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.MovieDirectionDTO;
import project.moviereza.model.MovieDirection;
import project.moviereza.repository.MovieDirectionRepository;

@RestController
@RequestMapping("/api")
public class MovieDirectionRestController {

	@Autowired
	private MovieDirectionRepository movieDirectionRepository;
	
//	Melihat list moviesdirection
	@GetMapping("/moviesdirection/all")
	public HashMap<String, Object> getAllMoviesDirection() {
		
		HashMap<String, Object> hasMovDir = new HashMap<String, Object>();
		ArrayList<MovieDirectionDTO> listMovDirDto = new ArrayList<MovieDirectionDTO>();
		
		for (MovieDirection movDir: movieDirectionRepository.findAll()) {
			MovieDirectionDTO movDirDto = new MovieDirectionDTO(movDir.getId(), movDir.getMovie(), movDir.getDirector());
				listMovDirDto.add(movDirDto);
		}
		
		hasMovDir.put("Message", "Show All Data");
		hasMovDir.put("Total", listMovDirDto.size());
		hasMovDir.put("Data", listMovDirDto);
		
	return hasMovDir;
	}
	
//	Melihat moviesdirection berdasarkan id
	@GetMapping("/moviesdirection/{id}")
	public HashMap<String, Object> getMoviesDirectionById(@PathVariable(value="id") Long id) {
		
		HashMap<String, Object> hasMovDir = new HashMap<String, Object>();
		
		MovieDirection movDir = movieDirectionRepository.findById(id).orElse(null);
		
		MovieDirectionDTO movDirDto = new MovieDirectionDTO(movDir.getId(), movDir.getMovie(), movDir.getDirector());
		
		hasMovDir.put("Message" , "Data By Id");
		hasMovDir.put("Data", movDirDto);
		
	return hasMovDir;
	}
	
//	Menghapus moviesdirection
	@DeleteMapping("/moviesdirection/delete/{id}")
	public HashMap<String, Object> deleteMovieDirectionById(@PathVariable(value="id") Long id) {
		
		HashMap<String, Object> hasMovDir = new HashMap<String, Object>();
		
		MovieDirection movDir = movieDirectionRepository.findById(id).orElse(null);
		
		movieDirectionRepository.delete(movDir);
		
		hasMovDir.put("Message", "Delete success");
		hasMovDir.put("Data", movDir);
		
	return hasMovDir;
	}
	
//	Menambahkan sebuah moviesdirection
	@PostMapping("/moviesdirection/add")
	public HashMap<String, Object> addMovieDirection(@Valid @RequestBody MovieDirectionDTO movieDirectionDTO) {
		
		HashMap<String, Object> hasMovDir = new HashMap<String, Object>();
		
		MovieDirection movDir = new MovieDirection(movieDirectionDTO.getId(), movieDirectionDTO.getMovie(), movieDirectionDTO.getDirector());
	
		hasMovDir.put("Message", "Add success");
		hasMovDir.put("Data", movieDirectionRepository.save(movDir));
		
	return hasMovDir;
	}
	
//	Menambahkan banyak moviesdirection
	@PostMapping("/moviesdirection/add/more")
	public HashMap<String, Object> addMoreMoviesDirection(@Valid @RequestBody MovieDirectionDTO...movieDirectionDTOs) {

		HashMap<String, Object> hasMovDir = new HashMap<String, Object>();
		ArrayList<MovieDirection> listMovDir = new ArrayList<MovieDirection>();
		
		for (MovieDirectionDTO movDirDto : movieDirectionDTOs) {
			MovieDirection movDir = new MovieDirection(movDirDto.getId(), movDirDto.getMovie(), movDirDto.getDirector());
			
			movieDirectionRepository.save(movDir);
			listMovDir.add(movDir);						
		}
		
		hasMovDir.put("Message" , "Add Success");
		hasMovDir.put("Total Insert", listMovDir.size());
		hasMovDir.put("Data", listMovDir);
		
	return hasMovDir;
	}
	
//	Update data moviesdirection
	@PutMapping("/moviesdirection/update/{id}")
	public HashMap<String, Object> updateMovieDirection(@PathVariable(value="id") Long id, @Valid @RequestBody MovieDirectionDTO movieDirectionDTO) {
		
		HashMap<String, Object> hasMovDir = new HashMap<String, Object>();
		
		MovieDirection movDir = movieDirectionRepository.findById(id).orElse(null);
		
		movieDirectionDTO.setId(movDir.getId());
		
		if (movieDirectionDTO.getMovie() != null) {
			movDir.setMovie(movieDirectionDTO.getMovie());
		}
		
		if (movieDirectionDTO.getDirector() != null) {
			movDir.setDirector(movieDirectionDTO.getDirector());
		}
		
		hasMovDir.put("Message", "Update success");
		hasMovDir.put("Data", movieDirectionRepository.save(movDir));
		
	return hasMovDir;
	}
}
