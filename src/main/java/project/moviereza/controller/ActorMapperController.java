package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.ActorDTO;
import project.moviereza.model.Actor;
import project.moviereza.repository.ActorRepository;

@RestController
@RequestMapping("/modrepapi")
public class ActorMapperController {

	@Autowired
	private ActorRepository actorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert entity To DTO
	private ActorDTO convertToDTO(Actor actor) {
		ActorDTO actDto = modelMapper.map(actor, ActorDTO.class);
	return actDto;
	}
	
//	Convert DTO To Entity
	private Actor convertToEntity(ActorDTO actorDTO) {
		Actor act = modelMapper.map(actorDTO, Actor.class);
	return act;
	}
	
//	Melihat list Actors
	@GetMapping("/actors/all")
	public HashMap<String, Object> getAllActors() {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
		ArrayList<ActorDTO> listActorDto = new ArrayList<ActorDTO>();
		
		for (Actor act: actorRepository.findAll()) {
							//	Mapping dengan model mapper	
			ActorDTO actDto = convertToDTO(act);
			listActorDto.add(actDto);
		}
		
		hashActors.put("Message", "Show All Data");
		hashActors.put("Total", listActorDto.size());
		hashActors.put("Data", listActorDto);
		
	return hashActors;
	}
	
//	Melihat actor berdasarkan id
	@GetMapping("/actors/{id}")
	public HashMap<String, Object> getActorById(@PathVariable(value="id") Long actId) {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
						// Mapping dengan model mapper	
		ActorDTO actDto = convertToDTO(act);
		
		hashActors.put("Message" , "Data By Id");
		hashActors.put("Data", actDto);
		
	return hashActors;
	}
	
//	Menambahkan seorang actor
	@PostMapping("/actors/add")
	public HashMap<String, Object> addActor(@Valid @RequestBody ActorDTO actorDTO) {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
					//Mapping dengan model mapper	
		Actor act = convertToEntity(actorDTO);
	
		
		hashActors.put("Message", "Add success");
		hashActors.put("Data", actorRepository.save(act));
		
	return hashActors;
	}
	
//	Update data actors
	@PutMapping("/actors/update/{id}")
	public HashMap<String, Object> updateActor(@PathVariable(value="id") Long actId, @Valid @RequestBody ActorDTO actorDTO) {
		
		HashMap<String, Object> hasActor = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
		
		actorDTO.setActId(act.getActId());
		
		//	Mapping dengan model mapper
		if(actorDTO.getActFname() != null) {
			act.setActFname(convertToEntity(actorDTO).getActFname());
		}
		if(actorDTO.getActLname() != null) {
			act.setActLname(convertToEntity(actorDTO).getActLname());
		}
		if(actorDTO.getActGender() != null) {
			act.setActGender(convertToEntity(actorDTO).getActGender());
		}
		
		
		
		
	
		
		hasActor.put("Message", "Update success");
		hasActor.put("Data", actorRepository.save(act));
		
	return hasActor;
	}
	
//	Menghapus actor
	@DeleteMapping("/actors/delete/{id}")
	public HashMap<String, Object> deleteActorById(@PathVariable(value="id") Long actId) {
		
		HashMap<String, Object> hashActors = new HashMap<String, Object>();
		
		Actor act = actorRepository.findById(actId).orElse(null);
		
		actorRepository.delete(act);
		
		hashActors.put("Message", "Delete success");
		hashActors.put("Data",act);
		
	return hashActors;
	}
	
	
}
