package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.MovieGenreDTO;
import project.moviereza.model.MovieGenre;
import project.moviereza.repository.MovieGenreRepository;

@RestController
@RequestMapping("/api")
public class MovieGenreRestController {

	@Autowired
	private MovieGenreRepository movieGenreRepository;
	
//	Melihat list movie genre
	@GetMapping("/moviesgenre/all")
	public HashMap<String, Object> getAllMoviesGenre() {
		
		HashMap<String, Object> hasMovGen = new HashMap<String, Object>();
		ArrayList<MovieGenreDTO> listMovGenDto = new ArrayList<MovieGenreDTO>();
		
		for (MovieGenre movGen: movieGenreRepository.findAll()) {
			MovieGenreDTO movGenDto = new MovieGenreDTO(movGen.getId(), movGen.getMovie(), movGen.getGenre());
					listMovGenDto.add(movGenDto);
		}
		
		hasMovGen.put("Message", "Show All Data");
		hasMovGen.put("Total", listMovGenDto.size());
		hasMovGen.put("Data", listMovGenDto);
		
	return hasMovGen;
	}
	
//	Melihat moviesgenre berdasarkan id
	@GetMapping("/moviesgenre/{id}")
	public HashMap<String, Object> getMoviesGenreById(@PathVariable(value="id") Long id) {
		
		HashMap<String, Object> hasMovGen = new HashMap<String, Object>();
		
		MovieGenre movGen = movieGenreRepository.findById(id).orElse(null);
		
		MovieGenreDTO movGenDto = new MovieGenreDTO(movGen.getId(), movGen.getMovie(), movGen.getGenre());
		
		hasMovGen.put("Message" , "Data By Id");
		hasMovGen.put("Data", movGenDto);
		
	return hasMovGen;
	}
	
//	Menghapus moviesgenre
	@DeleteMapping("/moviesgenre/delete/{id}")
	public HashMap<String, Object> deleteMovieGenreById(@PathVariable(value="id") Long id) {
		
		HashMap<String, Object> hasMovGen = new HashMap<String, Object>();
		
		MovieGenre movGen = movieGenreRepository.findById(id).orElse(null);
		
		movieGenreRepository.delete(movGen);
		
		hasMovGen.put("Message", "Delete success");
		hasMovGen.put("Data", movGen);
		
	return hasMovGen;
	}
	
//	Menambahkan sebuah moviesgenre
	@PostMapping("/moviesgenre/add")
	public HashMap<String, Object> addMovieGenre(@Valid @RequestBody MovieGenreDTO movieGenreDTO) {
		
		HashMap<String, Object> hasMovGen = new HashMap<String, Object>();
		
		MovieGenre movGen = new MovieGenre(movieGenreDTO.getId(), movieGenreDTO.getMovie(), movieGenreDTO.getGenre());
	
		hasMovGen.put("Message", "Add success");
		hasMovGen.put("Data", movieGenreRepository.save(movGen));
		
	return hasMovGen;
	}
	
//	Menambahkan banyak moviesgenre
	@PostMapping("/moviesgenre/add/more")
	public HashMap<String, Object> addMoreMovieGenre(@Valid @RequestBody MovieGenreDTO...movieCastDTOs) {

		HashMap<String, Object> hasMovGen = new HashMap<String, Object>();
		ArrayList<MovieGenre> listMovGen = new ArrayList<MovieGenre>();
		
		for (MovieGenreDTO movCGenDto : movieCastDTOs) {
			MovieGenre movGen = new MovieGenre(movCGenDto.getId(), movCGenDto.getMovie(), movCGenDto.getGenre());
			
			movieGenreRepository.save(movGen);
			listMovGen.add(movGen);						
		}
		
		hasMovGen.put("Message" , "Add Success");
		hasMovGen.put("Total Insert", listMovGen.size());
		hasMovGen.put("Data", listMovGen);
		
	return hasMovGen;
	}
	
//	Update data moviesgenre
	@PutMapping("/moviesgenre/update/{id}")
	public HashMap<String, Object> updateMovieGenre(@PathVariable(value="id") Long id, @Valid @RequestBody MovieGenreDTO movieGenreDTO) {
		
		HashMap<String, Object> hasMovGen = new HashMap<String, Object>();
		
		MovieGenre movGen = movieGenreRepository.findById(id).orElse(null);
		
		movieGenreDTO.setId(movGen.getId());
		
		if (movieGenreDTO.getMovie() != null) {
			movGen.setMovie(movieGenreDTO.getMovie());
		}
		
		if (movieGenreDTO.getGenre() != null) {
			movGen.setGenre(movieGenreDTO.getGenre());
		}
		
		
		hasMovGen.put("Message", "Update success");
		hasMovGen.put("Data", movieGenreRepository.save(movGen));
		
	return hasMovGen;
	}
	
}
