package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.ReviewerDTO;
import project.moviereza.model.Reviewer;
import project.moviereza.repository.ReviewerRepository;

@RestController
@RequestMapping("/modrepapi")
public class ReviewerMapperController {

	
	@Autowired
	private ReviewerRepository reviewerRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Convert entity To DTO
	private ReviewerDTO convertToDTO(Reviewer reviewer) {
		ReviewerDTO revDto = modelMapper.map(reviewer, ReviewerDTO.class);
	return revDto;
	}
	
//	Convert DTO To Entity
	private Reviewer convertToEntity(ReviewerDTO reviewerDTO) {
		Reviewer reviewer = modelMapper.map(reviewerDTO, Reviewer.class);
	return reviewer;
	}
	
	
//	Melihat list Reviewer
	@GetMapping("/reviewers/all")
	public HashMap<String, Object> getAllReviewers() {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		ArrayList<ReviewerDTO> listReviewerDto = new ArrayList<ReviewerDTO>();
		
		for (Reviewer rev: reviewerRepository.findAll()) {
								// Mapping dengan model mapper	
			listReviewerDto.add(convertToDTO(rev));
		}
		
		hasReviewer.put("Message", "Show All Data");
		hasReviewer.put("Total", listReviewerDto.size());
		hasReviewer.put("Data", listReviewerDto);
		
	return hasReviewer;
	}
	
//	Melihat Reviewer berdasarkan id
	@GetMapping("/reviewers/{id}")
	public HashMap<String, Object> getReviewerById(@PathVariable(value="id") Long revId) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
							// Mapping dengan model mapper	
		ReviewerDTO revDto = convertToDTO(rev);
		
		hasReviewer.put("Message" , "Data By Id");
		hasReviewer.put("Data", revDto);
		
	return hasReviewer;
	}
	
//	Menambahkan seorang reviewer
	@PostMapping("/reviewers/add")
	public HashMap<String, Object> addReviewer(@Valid @RequestBody ReviewerDTO reviewerDTO) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
					// Mapping dengan model mapper	
		Reviewer rev = convertToEntity(reviewerDTO);
	
		hasReviewer.put("Message", "Add success");
		hasReviewer.put("Data", reviewerRepository.save(rev));
		
	return hasReviewer;
	}

	
//	Update data Reviewer
	@PutMapping("/reviewers/update/{id}")
	public HashMap<String, Object> updateReviewer(@PathVariable(value="id") Long revId, @Valid @RequestBody ReviewerDTO reviewerDTO) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
//		Mapping dengan model mapper	
		rev.setRevName(convertToEntity(reviewerDTO).getRevName());
		
		
		hasReviewer.put("Message", "Update success");
		hasReviewer.put("Data", reviewerRepository.save(rev));
		
	return hasReviewer;
	}
	
//	Menghapus reviewer
	@DeleteMapping("/reviewers/delete/{id}")
	public HashMap<String, Object> deleteReviewerById(@PathVariable(value="id") Long revId) {
		
		HashMap<String, Object> hasReviewer = new HashMap<String, Object>();
		
		Reviewer rev = reviewerRepository.findById(revId).orElse(null);
		
		reviewerRepository.delete(rev);
		
		hasReviewer.put("Message", "Delete success");
		hasReviewer.put("Data", rev);
		
	return hasReviewer;
	}
}
