package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.MovieDTO;
import project.moviereza.model.Movie;
import project.moviereza.repository.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieRestController {

	@Autowired
	private MovieRepository movieRepository;
	
//	Melihat list Movie
	@GetMapping("/movies/all")
	public HashMap<String, Object> getAllMovies() {
		
		HashMap<String, Object> hasMovie = new HashMap<String, Object>();
		ArrayList<MovieDTO> listMovieDto = new ArrayList<MovieDTO>();
		
		for (Movie mov : movieRepository.findAll()) {
			MovieDTO movDto = new MovieDTO(mov.getMovId(), mov.getMovTitle(), mov.getYear(), mov.getMovTime(), mov.getMovLang(), mov.getMovDtRel(), mov.getMovRelCountry());
			listMovieDto.add(movDto);
		}
		
		hasMovie.put("Message", "Show All Data");
		hasMovie.put("Total", listMovieDto.size());
		hasMovie.put("Data", listMovieDto);
		
	return hasMovie;
	}
	
//	Melihat Movie berdasarkan id
	@GetMapping("/movies/{id}")
	public HashMap<String, Object> getMovieById(@PathVariable(value="id") Long movId) {
		
		HashMap<String, Object> hasMovie = new HashMap<String, Object>();
		
		Movie mov = movieRepository.findById(movId).orElse(null);
		
		MovieDTO movDto = new MovieDTO(mov.getMovId(), mov.getMovTitle(), mov.getYear(), mov.getMovTime(), mov.getMovLang(), mov.getMovDtRel(), mov.getMovRelCountry());
		
		hasMovie.put("Message" , "Data By Id");
		hasMovie.put("Data", movDto);
		
	return hasMovie;
	}
	
//	Menghapus Movie
	@DeleteMapping("/movies/delete/{id}")
	public HashMap<String, Object> deleteMovieById(@PathVariable(value="id") Long movId) {
		
		HashMap<String, Object> hasMovie = new HashMap<String, Object>();
		
		Movie mov = movieRepository.findById(movId).orElse(null);
		
		movieRepository.delete(mov);
		
		hasMovie.put("Message", "Delete success");
		hasMovie.put("Data",mov);
		
	return hasMovie;
	}
	
//	Menambahkan Sebuah movie
	@PostMapping("/movies/add")
	public HashMap<String, Object> addMovie(@Valid @RequestBody MovieDTO movieDTO) {
		
		HashMap<String, Object> hasMovie = new HashMap<String, Object>();
		
		Movie mov = new Movie(movieDTO.getMovId(),movieDTO.getMovTitle(), movieDTO.getYear(), movieDTO.getMovTime(), movieDTO.getMovLang(), movieDTO.getMovDtRel(), movieDTO.getMovRelCountry());
	
		hasMovie.put("Message", "Add success");
		hasMovie.put("Data", movieRepository.save(mov));
		
	return hasMovie;
	}

//	Menambahkan banyak movie
	@PostMapping("/movies/add/more")
	public HashMap<String, Object> addMoreMovies(@Valid @RequestBody MovieDTO...movieDTOs) {
		
		HashMap<String, Object> hasMovie = new HashMap<String, Object>();
		ArrayList<Movie> listMovies = new ArrayList<Movie>();
		
		for (MovieDTO movDto : movieDTOs) {
			Movie mov = new Movie(movDto.getMovId(), movDto.getMovTitle(), movDto.getYear(), movDto.getMovTime(), movDto.getMovLang(), movDto.getMovDtRel(), movDto.getMovRelCountry());
			
			movieRepository.save(mov);
			listMovies.add(mov);						
		}
		
		hasMovie.put("Message" , "Add Success");
		hasMovie.put("Total Insert", listMovies.size());
		hasMovie.put("Data", listMovies);
		
	return hasMovie;
	}
	
//	Update data Movie
	@PutMapping("/movies/update/{id}")
	public HashMap<String, Object> updateMovie(@PathVariable(value="id") Long movId, @Valid @RequestBody MovieDTO movieDTO) {
		
		HashMap<String, Object> hasMovie = new HashMap<String, Object>();
		
		Movie mov = movieRepository.findById(movId).orElse(null);
		
		
		movieDTO.setMovId(mov.getMovId());
		
		if (movieDTO.getMovTitle() != null) {
			mov.setMovTitle(movieDTO.getMovTitle());
		}
		
		if (movieDTO.getYear() != 0) {
			mov.setYear(movieDTO.getYear());
		}
		
		if (movieDTO.getMovTime() != 0) {
			mov.setMovTime(movieDTO.getMovTime());
		}
		
		if (movieDTO.getMovLang() != null) {
			mov.setMovLang(movieDTO.getMovLang());
		}
		
		if (movieDTO.getMovDtRel() != null) {
			mov.setMovDtRel(movieDTO.getMovDtRel());
		}
		
		if (movieDTO.getMovRelCountry() != null) {
			mov.setMovRelCountry(movieDTO.getMovRelCountry());
		}
		
		hasMovie.put("Message", "Update success");
		hasMovie.put("Data", movieRepository.save(mov));
		
	return hasMovie;
	}
}
