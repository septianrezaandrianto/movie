package project.moviereza.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.moviereza.dto.MovieCastDTO;
import project.moviereza.model.MovieCast;
import project.moviereza.repository.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastRestController {
	
	@Autowired
	private MovieCastRepository movieCastRepository;
	
//	Melihat list moviescast
	@GetMapping("/moviescast/all")
	public HashMap<String, Object> getAllMoviesCast() {
		
		HashMap<String, Object> hasMovCast = new HashMap<String, Object>();
		ArrayList<MovieCastDTO> listMovCastDto = new ArrayList<MovieCastDTO>();
		
		for (MovieCast movCast: movieCastRepository.findAll()) {
			MovieCastDTO movCastDto = new MovieCastDTO(movCast.getId(), movCast.getActor(), movCast.getMovie(), movCast.getRole());
			listMovCastDto.add(movCastDto);
		}
		
		hasMovCast.put("Message", "Show All Data");
		hasMovCast.put("Total", listMovCastDto.size());
		hasMovCast.put("Data", listMovCastDto);
		
	return hasMovCast;
	}
	
//	Melihat moviescast berdasarkan id
	@GetMapping("/moviescast/{id}")
	public HashMap<String, Object> getMoviesCastById(@PathVariable(value="id") Long id) {
		
		HashMap<String, Object> hasMovCast = new HashMap<String, Object>();
		
		MovieCast movCast = movieCastRepository.findById(id).orElse(null);
		
		MovieCastDTO movCastDto = new MovieCastDTO(movCast.getId(),movCast.getActor(), movCast.getMovie(), movCast.getRole());
		
		hasMovCast.put("Message" , "Data By Id");
		hasMovCast.put("Data", movCastDto);
		
	return hasMovCast;
	}

//	Menghapus MovieCast
	@DeleteMapping("/moviescast/delete/{id}")
	public HashMap<String, Object> deleteMovieCastById(@PathVariable(value="id") Long id) {
		
		HashMap<String, Object> hasMovCast = new HashMap<String, Object>();
		
		MovieCast movCast = movieCastRepository.findById(id).orElse(null);
		
		movieCastRepository.delete(movCast);
		
		hasMovCast.put("Message", "Delete success");
		hasMovCast.put("Data", movCast);
		
	return hasMovCast;
	}
	
//	Menambahkan sebuah Moviescast
	@PostMapping("/moviescast/add")
	public HashMap<String, Object> addMovieCast(@Valid @RequestBody MovieCastDTO movieCastDTO) {
		
		HashMap<String, Object> hasMovCast = new HashMap<String, Object>();
		
		MovieCast movCast = new MovieCast(movieCastDTO.getId(), movieCastDTO.getActor(), movieCastDTO.getMovie(), movieCastDTO.getRole());
	
		hasMovCast.put("Message", "Add success");
		hasMovCast.put("Data", movieCastRepository.save(movCast));
		
	return hasMovCast;
	}
	
//	Menambahkan banyak Moviescast
	@PostMapping("/moviescast/add/more")
	public HashMap<String, Object> addMoreMovieCast(@Valid @RequestBody MovieCastDTO...movieCastDTOs) {
		
		HashMap<String, Object> hasMovCast = new HashMap<String, Object>();
		ArrayList<MovieCast> listMovCast = new ArrayList<MovieCast>();
		
		for (MovieCastDTO movCastDto : movieCastDTOs) {
			MovieCast movCast = new MovieCast(movCastDto.getId(), movCastDto.getActor(), movCastDto.getMovie(), movCastDto.getRole());
			
			movieCastRepository.save(movCast);
			listMovCast.add(movCast);						
		}
		
		hasMovCast.put("Message" , "Add Success");
		hasMovCast.put("Total Insert", listMovCast.size());
		hasMovCast.put("Data", listMovCast);
		
	return hasMovCast;
	}
	
//	Update data Moviescast
	@PutMapping("/moviescast/update/{id}")
	public HashMap<String, Object> updateMovieCast(@PathVariable(value="id") Long id, @Valid @RequestBody MovieCastDTO movieCastDTO) {
		
		HashMap<String, Object> hasMovCast = new HashMap<String, Object>();
		
		MovieCast movCast = movieCastRepository.findById(id).orElse(null);
		
		movCast.setId(movCast.getId());
		
		if (movieCastDTO.getActor() != null) {
			movCast.setActor(movieCastDTO.getActor());
		}
		if (movieCastDTO.getMovie() != null) {
			movCast.setMovie(movieCastDTO.getMovie());
		}
		if (movieCastDTO.getRole() != null) {
			movCast.setRole(movieCastDTO.getRole());
		}
		
		hasMovCast.put("Message", "Update success");
		hasMovCast.put("Data", movieCastRepository.save(movCast));
		
	return hasMovCast;
	}
	
}
