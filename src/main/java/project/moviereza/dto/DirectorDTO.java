package project.moviereza.dto;

public class DirectorDTO {
	
	private Long dirId;
	private String dirFname;
	private String dirLname;
	
	public DirectorDTO() {
		super();
	}

	public DirectorDTO(Long dirId, String dirFname, String dirLname) {
		super();
		this.dirId = dirId;
		this.dirFname = dirFname;
		this.dirLname = dirLname;
	}

	public Long getDirId() {
		return dirId;
	}

	public void setDirId(Long dirId) {
		this.dirId = dirId;
	}

	public String getDirFname() {
		return dirFname;
	}

	public void setDirFname(String dirFname) {
		this.dirFname = dirFname;
	}

	public String getDirLname() {
		return dirLname;
	}

	public void setDirLname(String dirLname) {
		this.dirLname = dirLname;
	}
	
	
}
