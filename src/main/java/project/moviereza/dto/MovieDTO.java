package project.moviereza.dto;

import java.util.Date;

public class MovieDTO {

	private Long movId;
	private String movTitle;
	private int year;
	private int movTime;
	private String movLang;
	private Date movDtRel;
	private String movRelCountry;
		
	public MovieDTO() {
		super();
	}
	
	public MovieDTO(Long movId, String movTitle, int year, int movTime, String movLang, Date movDtRel,
			String movRelCountry) {
		super();
		this.movId = movId;
		this.movTitle = movTitle;
		this.year = year;
		this.movTime = movTime;
		this.movLang = movLang;
		this.movDtRel = movDtRel;
		this.movRelCountry = movRelCountry;
	}
	
	public Long getMovId() {
		return movId;
	}
	public void setMovId(Long movId) {
		this.movId = movId;
	}
	public String getMovTitle() {
		return movTitle;
	}
	public void setMovTitle(String movTitle) {
		this.movTitle = movTitle;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMovTime() {
		return movTime;
	}
	public void setMovTime(int movTime) {
		this.movTime = movTime;
	}
	public String getMovLang() {
		return movLang;
	}
	public void setMovLang(String movLang) {
		this.movLang = movLang;
	}
	public Date getMovDtRel() {
		return movDtRel;
	}
	public void setMovDtRel(Date movDtRel) {
		this.movDtRel = movDtRel;
	}
	public String getMovRelCountry() {
		return movRelCountry;
	}
	public void setMovRelCountry(String movRelCountry) {
		this.movRelCountry = movRelCountry;
	}
	
	
}
