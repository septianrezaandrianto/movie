package project.moviereza.dto;

import project.moviereza.model.Director;
import project.moviereza.model.Movie;
import project.moviereza.model.MovieDirectionKey;

public class MovieDirectionDTO {
	
	private MovieDirectionKey id;
	private Movie movie;
	private Director director;
	
	
	public MovieDirectionDTO() {
		super();
	}
	
	public MovieDirectionDTO(MovieDirectionKey id, Movie movie, Director director) {
		super();
		this.id = id;
		this.movie = movie;
		this.director = director;
	}
	
	
	public MovieDirectionKey getId() {
		return id;
	}
	public void setId(MovieDirectionKey id) {
		this.id = id;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public Director getDirector() {
		return director;
	}
	public void setDirector(Director director) {
		this.director = director;
	}
	
	
}
