package project.moviereza.dto;

import project.moviereza.model.Genre;
import project.moviereza.model.Movie;
import project.moviereza.model.MovieGenreKey;

public class MovieGenreDTO {

	private MovieGenreKey id;
	private Movie movie;
	private Genre genre;
	
	public MovieGenreDTO() {
		super();
	}

	public MovieGenreDTO(MovieGenreKey id, Movie movie, Genre genre) {
		super();
		this.id = id;
		this.movie = movie;
		this.genre = genre;
	}

	public MovieGenreKey getId() {
		return id;
	}

	public void setId(MovieGenreKey id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	
}
