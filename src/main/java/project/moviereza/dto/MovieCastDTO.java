package project.moviereza.dto;

import project.moviereza.model.Actor;
import project.moviereza.model.Movie;
import project.moviereza.model.MovieCastKey;

public class MovieCastDTO {

	private MovieCastKey id;
	private Actor actor;
	private Movie movie;
	private String role;
	
	public MovieCastDTO() {
		super();
	}

	public MovieCastDTO(MovieCastKey id, Actor actor, Movie movie, String role) {
		super();
		this.id = id;
		this.actor = actor;
		this.movie = movie;
		this.role = role;
	}


	public MovieCastKey getId() {
		return id;
	}

	public void setId(MovieCastKey id) {
		this.id = id;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}


	
	
	
}
