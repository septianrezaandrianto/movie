package project.moviereza.dto;

public class ReviewerDTO {

	private Long revId;
	private String revName;
	
	public ReviewerDTO() {
		super();
	}
	
	public ReviewerDTO(Long revId, String revName) {
		super();
		this.revId = revId;
		this.revName = revName;
	}
	public Long getRevId() {
		return revId;
	}
	public void setRevId(Long revId) {
		this.revId = revId;
	}
	public String getRevName() {
		return revName;
	}
	public void setRevName(String revName) {
		this.revName = revName;
	}
	
	
}
