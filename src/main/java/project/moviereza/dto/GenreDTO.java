package project.moviereza.dto;

public class GenreDTO {

	private Long genId;
	
	private String genTitle;

	public GenreDTO() {
		super();
	}

	public GenreDTO(Long genId, String genTitle) {
		super();
		this.genId = genId;
		this.genTitle = genTitle;
	}

	public Long getGenId() {
		return genId;
	}

	public void setGenId(Long genId) {
		this.genId = genId;
	}

	public String getGenTitle() {
		return genTitle;
	}

	public void setGenTitle(String genTitle) {
		this.genTitle = genTitle;
	}
	
	
}
