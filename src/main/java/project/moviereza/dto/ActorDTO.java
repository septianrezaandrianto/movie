package project.moviereza.dto;

public class ActorDTO {

	private Long actId;
	private String actFname;
	private String actLname;
	private String actGender;
	
	public ActorDTO() {
		super();
	}
	
	public ActorDTO(Long actId, String actFname, String actLname, String actGender) {
		super();
		this.actId = actId;
		this.actFname = actFname;
		this.actLname = actLname;
		this.actGender = actGender;
	}
	
	public Long getActId() {
		return actId;
	}
	public void setActId(Long actId) {
		this.actId = actId;
	}
	public String getActFname() {
		return actFname;
	}
	public void setActFname(String actFname) {
		this.actFname = actFname;
	}
	public String getActLname() {
		return actLname;
	}
	public void setActLname(String actLname) {
		this.actLname = actLname;
	}
	public String getActGender() {
		return actGender;
	}
	public void setActGender(String actGender) {
		this.actGender = actGender;
	}
	
	
}
