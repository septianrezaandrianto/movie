package project.moviereza.dto;

import project.moviereza.model.Movie;
import project.moviereza.model.RatingKey;
import project.moviereza.model.Reviewer;

public class RatingDTO {
	
	private RatingKey id;
	private Movie movie;
	private Reviewer reviewer;
	private int revStars;
	private int numORatings;
	
	public RatingDTO() {
		super();
	}
	
	public RatingDTO(RatingKey id, Movie movie, Reviewer reviewer, int revStars, int numORatings) {
		super();
		this.id = id;
		this.movie = movie;
		this.reviewer = reviewer;
		this.revStars = revStars;
		this.numORatings = numORatings;
	}

	
	public RatingKey getId() {
		return id;
	}

	public void setId(RatingKey id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public Reviewer getReviewer() {
		return reviewer;
	}
	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}
	public int getRevStars() {
		return revStars;
	}
	public void setRevStars(int revStars) {
		this.revStars = revStars;
	}
	public int getNumORatings() {
		return numORatings;
	}
	public void setNumORatings(int numORatings) {
		this.numORatings = numORatings;
	}
	

}
