package project.moviereza.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="directors")
public class Director {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="dir_id")
	private Long dirId;
	
	@Column(name="dir_fname", nullable = false)
	private String dirFname;

	@Column(name="dir_lname", nullable = false)
	private String dirLname;
	
	@OneToMany
	(mappedBy = "director")
	Set<MovieDirection> direct;

	public Director() {
		super();
	}

	public Director(Long dirId, String dirFname, String dirLname) {
		super();
		this.dirId = dirId;
		this.dirFname = dirFname;
		this.dirLname = dirLname;
	}

	public Long getDirId() {
		return dirId;
	}

	public void setDirId(Long dirId) {
		this.dirId = dirId;
	}

	public String getDirFname() {
		return dirFname;
	}

	public void setDirFname(String dirFname) {
		this.dirFname = dirFname;
	}

	public String getDirLname() {
		return dirLname;
	}

	public void setDirLname(String dirLname) {
		this.dirLname = dirLname;
	}


}
