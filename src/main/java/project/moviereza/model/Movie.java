package project.moviereza.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "movies")
public class Movie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="mov_id")
	private Long movId;
	
	@Column(name="mov_title", nullable = false)
	private String movTitle;
	
	@Column(name="year", nullable = false)
	private int year;
	
	@Column(name="mov_time", nullable = false)
	private int movTime;
	
	@Column(name="mov_lang", nullable = false)
	private String movLang;
	
	@Column(name="mov_dt_rel", nullable = false)
	@JsonFormat(pattern="dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	private Date movDtRel;
	
	@Column(name="mov_rel_country", nullable = false)
	private String movRelCountry;
	

//	Join column movie - movie direction
	@OneToMany
	(mappedBy = "movie")
	Set<MovieDirection> direct;
	
//	Join column movie - movie genre
	@OneToMany
	(mappedBy = "movie")
	Set<MovieGenre> gen;
	
		
//	Join column movie - rating
	@OneToMany
	(mappedBy = "movie")
	Set<Rating> ratings, numORatings;
	
//	Join Column movie - movie cast
	@OneToMany
	(mappedBy = "movie")
	private Set<MovieCast> role;
	
	
	public Movie() {
		super();
	}
	

	public Movie(Long movId, String movTitle, int year, int movTime, String movLang, Date movDtRel,
			String movRelCountry) {
		super();
		this.movId = movId;
		this.movTitle = movTitle;
		this.year = year;
		this.movTime = movTime;
		this.movLang = movLang;
		this.movDtRel = movDtRel;
		this.movRelCountry = movRelCountry;
	}


	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}

	public String getMovTitle() {
		return movTitle;
	}

	public void setMovTitle(String movTitle) {
		this.movTitle = movTitle;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMovTime() {
		return movTime;
	}

	public void setMovTime(int movTime) {
		this.movTime = movTime;
	}

	public String getMovLang() {
		return movLang;
	}

	public void setMovLang(String movLang) {
		this.movLang = movLang;
	}

	public Date getMovDtRel() {
		return movDtRel;
	}

	public void setMovDtRel(Date movDtRel) {
		this.movDtRel = movDtRel;
	}

	public String getMovRelCountry() {
		return movRelCountry;
	}

	public void setMovRelCountry(String movRelCountry) {
		this.movRelCountry = movRelCountry;
	}
	
}