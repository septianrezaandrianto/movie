package project.moviereza.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;

@Embeddable
public class MovieDirectionKey implements Serializable {
	
	@GeneratedValue
	@Column(name = "mov_id")
	private Long movId;
	
	@GeneratedValue
	@Column(name="dir_id")
	private Long dirId;

	
	public MovieDirectionKey() {
		super();
	}

	public MovieDirectionKey(Long movId, Long dirId) {
		super();
		this.movId = movId;
		this.dirId = dirId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}

	public Long getDirId() {
		return dirId;
	}

	public void setDirId(Long dirId) {
		this.dirId = dirId;
	}
	
	@Override
	public int hashCode() {
		int pk = 31;
		int result = 1;
		
		result = pk * result
				+ ((movId == null) ? 0 : movId.hashCode());
		result = pk * result
				+((dirId == null) ? 0 : dirId.hashCode());
	
	return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		
		MovieDirectionKey other = (MovieDirectionKey) obj;
	return Objects.equals(getMovId(), other.getMovId()) && Objects.equals(getDirId(), other.getDirId());
	}

}
