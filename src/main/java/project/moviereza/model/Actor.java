package project.moviereza.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="actors")
public class Actor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="act_id")
	private Long actId;
	
	@Column(name="act_fname", nullable = false)
	private String actFname;
	
	@Column(name="act_lname", nullable = false)
	private String actLname;
	
	@Column(name="act_gender", nullable = false)
	private String actGender;
	
	@OneToMany
	(mappedBy = "actor")
	Set<MovieCast> role;

	public Actor() {
		super();
	}


	public Actor(Long actId, String actFname, String actLname, String actGender) {
		super();
		this.actId = actId;
		this.actFname = actFname;
		this.actLname = actLname;
		this.actGender = actGender;

	}


	public Long getActId() {
		return actId;
	}

	public void setActId(Long actId) {
		this.actId = actId;
	}

	public String getActFname() {
		return actFname;
	}

	public void setActFname(String actFname) {
		this.actFname = actFname;
	}

	public String getActLname() {
		return actLname;
	}

	public void setActLname(String actLname) {
		this.actLname = actLname;
	}

	public String getActGender() {
		return actGender;
	}

	public void setActGender(String actGender) {
		this.actGender = actGender;
	}
	
	
}
