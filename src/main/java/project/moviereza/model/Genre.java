package project.moviereza.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="genres")
public class Genre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "gen_id")
	private Long genId;
	
	@Column(name="gen_title", nullable = false)
	private String genTitle;

	@OneToMany
	(mappedBy = "genre")
	Set<MovieGenre> gen;
	
	public Genre() {
		super();
	}

	public Genre(Long genId, String genTitle) {
		super();
		this.genId = genId;
		this.genTitle = genTitle;
	}

	public Long getGenId() {
		return genId;
	}

	public void setGenId(Long genId) {
		this.genId = genId;
	}

	public String getGenTitle() {
		return genTitle;
	}

	public void setGenTitle(String genTitle) {
		this.genTitle = genTitle;
	}
	
}
