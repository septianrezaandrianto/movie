package project.moviereza.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="movies_direction")
public class MovieDirection {
	
	@EmbeddedId
	private MovieDirectionKey id;
	
	@ManyToOne
	@MapsId("mov_id")
	@JoinColumn(name="mov_id")
	private Movie movie;
	
	@ManyToOne
	@MapsId("dir_id")
	@JoinColumn(name="dir_id")
	private Director director;

	public MovieDirection() {
		super();
	}


	public MovieDirection(MovieDirectionKey id, Movie movie, Director director) {
		super();
		this.id = id;
		this.movie = movie;
		this.director = director;
	}


	public MovieDirectionKey getId() {
		return id;
	}


	public void setId(MovieDirectionKey id) {
		this.id = id;
	}


	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	
}
