package project.moviereza.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="ratings")
public class Rating {
	
	@EmbeddedId
	private RatingKey id;
	
	@ManyToOne
	@MapsId("mov_id")
	@JoinColumn(name="mov_id")
	private Movie movie;
	
	@ManyToOne
	@MapsId("rev_id")
	@JoinColumn(name="rev_id")
	private Reviewer reviewer;
	
	@Column(name="rev_stars", nullable = false)
	private int revStars;
	
	@Column(name="num_o_ratings", nullable = false)
	private int numORatings;


	public Rating() {
		super();
	}


	public Rating(RatingKey id, Movie movie, Reviewer reviewer, int revStars, int numORatings) {
		super();
		this.id = id;
		this.movie = movie;
		this.reviewer = reviewer;
		this.revStars = revStars;
		this.numORatings = numORatings;
	}

	public RatingKey getId() {
		return id;
	}

	public void setId(RatingKey id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public int getRevStars() {
		return revStars;
	}

	public void setRevStars(int revStars) {
		this.revStars = revStars;
	}

	public int getNumORatings() {
		return numORatings;
	}

	public void setNumORatings(int numORatings) {
		this.numORatings = numORatings;
	}

	
	
}
