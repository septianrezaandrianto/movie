package project.moviereza.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="movies_cast")
public class MovieCast {
	
	@EmbeddedId
	private MovieCastKey id;
	

	@ManyToOne
	@MapsId("act_id")
	@JoinColumn(name="act_id")
	private Actor actor;
	

	@ManyToOne 
	@MapsId("mov_id")
	@JoinColumn(name="mov_id")
	private Movie movie;
	
	@Column(name ="role", nullable = false)
	private String role;

	
	public MovieCast() {
		super();
	}

	public MovieCast(MovieCastKey id, Actor actor, Movie movie, String role) {
		super();
		this.id = id;
		this.actor = actor;
		this.movie = movie;
		this.role = role;
	}



	public MovieCastKey getId() {
		return id;
	}

	public void setId(MovieCastKey id) {
		this.id = id;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	
}
