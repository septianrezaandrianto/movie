package project.moviereza.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="reviewers")
public class Reviewer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="rev_id")
	private Long revId;
	
	@Column(name="rev_name", nullable= false)
	private String revName;
	
	
	@OneToMany
	(mappedBy = "reviewer")
	private Set<Rating> revStars,  numORatings;

	public Reviewer() {
		super();
	}

	public Reviewer(Long revId, String revName) {
		super();
		this.revId = revId;
		this.revName = revName;
		
	}

	public Long getRevId() {
		return revId;
	}


	public void setRevId(Long revId) {
		this.revId = revId;
	}


	public String getRevName() {
		return revName;
	}


	public void setRevName(String revName) {
		this.revName = revName;
	}

	
}
