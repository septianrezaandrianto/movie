package project.moviereza.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;

@Embeddable
public class MovieCastKey implements Serializable {

	@GeneratedValue
	@Column(name="act_id")
	private Long actId;
	
	@GeneratedValue
	@Column(name="mov_id")
	private Long movId;

	public MovieCastKey() {
		super();
	}


	public MovieCastKey(Long actId, Long movId) {
		super();
		this.actId = actId;
		this.movId = movId;
	}

	public Long getActId() {
		return actId;
	}

	public void setActId(Long actId) {
		this.actId = actId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}
	
	@Override
	public int hashCode() {
		int pk = 31;
		int result =1;
		result = pk * result
				+ ((actId == null) ? 0 : actId.hashCode());
		result = pk * result
				+ ((movId == null) ? 0 : movId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(this == obj) {
			return true;
		}
			
		if (obj == null) {
			return false;
		}
		
		if(getClass() != obj.getClass()) {
			return false;
		}
		
		MovieCastKey other = (MovieCastKey) obj;
		return Objects.equals(getActId(), other.getActId()) && Objects.equals(getMovId(), other.getMovId());
	}
		
	
}
