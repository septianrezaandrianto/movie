package project.moviereza.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="movies_genre")
public class MovieGenre {

	@EmbeddedId
	private MovieGenreKey id;
	
	@ManyToOne
	@MapsId("mov_id")
	@JoinColumn(name = "mov_id")
	private Movie movie;
	
	@ManyToOne
	@MapsId("gen_id")
	@JoinColumn(name="gen_id")
	private Genre genre;

	public MovieGenre() {
		super();
	}

	public MovieGenre(MovieGenreKey id, Movie movie, Genre genre) {
		super();
		this.id = id;
		this.movie = movie;
		this.genre = genre;
	}

	public MovieGenreKey getId() {
		return id;
	}

	public void setId(MovieGenreKey id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	
}
