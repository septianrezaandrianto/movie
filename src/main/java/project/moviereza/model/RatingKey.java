package project.moviereza.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;

@Embeddable
public class RatingKey implements Serializable{


	@GeneratedValue
	@Column(name ="mov_id")
	private Long movId;
	
	@GeneratedValue
	@Column(name="rev_id")
	private Long revId;

	public RatingKey() {
		super();
	}

	public RatingKey(Long movId, Long revId) {
		super();
		this.movId = movId;
		this.revId = revId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}

	public Long getRevId() {
		return revId;
	}

	public void setRevId(Long revId) {
		this.revId = revId;
	}
	
	@Override
	public int hashCode() {
		int pk = 31;
		int result =1;
		result = pk * result
				+ ((revId == null) ? 0 : revId.hashCode());
		result = pk * result
				+ ((movId == null) ? 0 : movId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(this == obj) {
			return true;
		}
			
		if (obj == null) {
			return false;
		}
		
		if(getClass() != obj.getClass()) {
			return false;
		}
		
		RatingKey other = (RatingKey) obj;
		return Objects.equals(getRevId(), other.getRevId()) && Objects.equals(getMovId(), other.getMovId());
	}
}
