package project.moviereza.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;

@Embeddable
public class MovieGenreKey implements Serializable {
	
	@GeneratedValue
	@Column(name = "mov_id")
	private Long movId;
	
	@GeneratedValue
	@Column(name = "gen_id")
	private Long genId;

	
	public MovieGenreKey() {
		super();
	}

	public MovieGenreKey(Long movId, Long genId) {
		super();
		this.movId = movId;
		this.genId = genId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}

	public Long getGenId() {
		return genId;
	}

	public void setGenId(Long genId) {
		this.genId = genId;
	}
	
	
	@Override
	public int hashCode() {
		int pk = 31;
		int result = 1;
		
		result = pk * result
				+ ((movId == null) ? 0 : movId.hashCode());
		result = pk * result
				+((genId == null) ? 0 : genId.hashCode());
	
	return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		
		MovieGenreKey other = (MovieGenreKey) obj;
	return Objects.equals(getMovId(), other.getMovId()) && Objects.equals(getGenId(), other.getGenId());
	}
	
}
